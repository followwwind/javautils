import io.kubernetes.client.custom.NodeMetrics;
import io.kubernetes.client.custom.NodeMetricsList;
import io.kubernetes.client.openapi.models.V1Pod;
import io.kubernetes.client.openapi.models.V1PodList;
import org.junit.Test;

import java.util.List;

public class K8sHelperTest {

    K8sHelper k8sHelper = K8sHelper.fromUrl("http://10.18.1.20:8080/");

    @Test
    public void testNs(){
        List<String> nsList = k8sHelper.listNs();
        System.out.println(nsList);
    }

    @Test
    public void testListPod(){
        V1PodList v1PodList = k8sHelper.listPod("default", "");
        for(V1Pod v1Pod : v1PodList.getItems()){
            System.out.println(v1Pod.getMetadata().getName());
        }
    }

    @Test
    public void testNodeMetric(){
        NodeMetricsList metricsList = k8sHelper.getNodeMetric();
        List<NodeMetrics> items = metricsList.getItems();
        for(NodeMetrics item : items){
            System.out.println(item);
        }
    }
}
