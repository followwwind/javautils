package domian;

import io.kubernetes.client.openapi.models.V1EnvVar;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *    deployment信息
 * </p>
 * @author wind
 * @date    2024/12/11 18:30
 * @version v1.0
 */
public class Deployment implements Serializable {

    /**
     * 命名空间
     */
    private String namespace;

    /**
     * 名称
     */
    private String name;

    /**
     * 镜像名称
     */
    private String image;

    /**
     * 端口
     */
    private Integer port;

    /**
     * cpu
     */
    private String cpu;

    /**
     * 内存
     */
    private String memory;

    /**
     * 副本
     */
    private int replicas;

    private String engineNO;

    /**
     * 镜像密钥
     */
    private String imagePullSecrets;

    /**
     * 标签
     */
    private Map<String, String> nodeSelector;

    /**
     * 挂载
     */
    private List<VolumeMount> volumeMountList;

    /**
     *
     */
    private List<V1EnvVar> envList;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public String getEngineNO() {
        return engineNO;
    }

    public void setEngineNO(String engineNO) {
        this.engineNO = engineNO;
    }

    public List<VolumeMount> getVolumeMountList() {
        return volumeMountList;
    }

    public void setVolumeMountList(List<VolumeMount> volumeMountList) {
        this.volumeMountList = volumeMountList;
    }

    public String getImagePullSecrets() {
        return imagePullSecrets;
    }

    public void setImagePullSecrets(String imagePullSecrets) {
        this.imagePullSecrets = imagePullSecrets;
    }

    public List<V1EnvVar> getEnvList() {
        return envList;
    }

    public void setEnvList(List<V1EnvVar> envList) {
        this.envList = envList;
    }

    public Map<String, String> getNodeSelector() {
        return nodeSelector;
    }

    public void setNodeSelector(Map<String, String> nodeSelector) {
        this.nodeSelector = nodeSelector;
    }

}
