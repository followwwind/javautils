package domian;

import io.kubernetes.client.openapi.models.V1KeyToPath;
import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *     k8s configMap配置文件
 * </p>
 * @author wind
 * @date    2024/12/11 18:30
 * @version v1.0
 */
public class ConfigMap implements Serializable {
    
    private String name;

    private List<V1KeyToPath> items;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<V1KeyToPath> getItems() {
        return items;
    }

    public void setItems(List<V1KeyToPath> items) {
        this.items = items;
    }

}
