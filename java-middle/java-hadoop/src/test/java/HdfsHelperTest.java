import org.junit.Test;

import java.util.List;

public class HdfsHelperTest {


    @Test
    public void testHdfs() throws Exception {
        String hdfsUrl = "hdfs://10.18.1.12:8020";
        hdfsUrl = "hdfs://172.16.162.27:8020";
        HdfsHelper hdfsHelper = HdfsHelper.fromUrl(hdfsUrl);
        List<String> list = hdfsHelper.listFiles("/");
        System.out.println(list);
    }

    @Test
    public void testKerberos() throws Exception {
        String krb5Tab = "G:/doc/aimodeler/dev/keytab/hdfs.keytab";
        String krb5Conf = "G:/doc/aimodeler/dev/keytab/krb5.conf";
        String krb5User = "hdfs/bdp-hadoop-1@feisuan.com";
        String hadoopDir = "G:/doc/aimodeler/dev/hadoop";
        HdfsHelper hdfsHelper = HdfsHelper.fromKrb5(hadoopDir, krb5User, krb5Tab, krb5Conf);
        List<String> list = hdfsHelper.listFiles("/");
        System.out.println(list);
    }
}
