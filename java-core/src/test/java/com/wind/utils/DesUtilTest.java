package com.wind.utils;

import com.wind.utils.cipher.DesUtil;
import org.junit.Test;

public class DesUtilTest {

    @Test
    public void testDes(){
        String key = "12345678";
        String str = "world";
        String enStr = DesUtil.encrypt(str, key);
        System.out.println(enStr);

        String deStr = DesUtil.decrypt(enStr, key);
        System.out.println(deStr);
    }
}
