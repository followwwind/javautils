package com.wind.utils;

import com.wind.utils.cipher.CipherUtil;
import org.junit.Test;

import java.util.Arrays;

public class CipherUtilTest {

    @Test
    public void testMd5(){
        byte[] bytes = CipherUtil.md5("hello");
        System.out.println("byte:" + Arrays.toString(bytes));

        String hexStr = CipherUtil.byte2Hex(bytes);
        System.out.println("byte2Hex:" + hexStr);

        byte[] bytes2 = CipherUtil.hex2Byte(hexStr);
        System.out.println("hex2Byte:" + Arrays.toString(bytes2));

        System.out.println("byte2Hex2:" + CipherUtil.byte2Hex2(bytes));


    }

    @Test
    public void testBase64(){
        String base64 = CipherUtil.encodeBase64("12333333333333333333333333333333333333");
        System.out.println(base64);
        System.out.println(CipherUtil.decodeBase64(base64));
    }
}
