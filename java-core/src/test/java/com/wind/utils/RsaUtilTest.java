package com.wind.utils;

import com.wind.utils.cipher.RsaUtil;
import org.junit.Test;

import java.util.List;

public class RsaUtilTest {

    @Test
    public void testRsa(){
        try {
            List<String> keyList = RsaUtil.genKeyPair();
            String publicKey = keyList.get(0);
            String privateKey = keyList.get(1);
            System.out.println("publicKey: " + publicKey);
            System.out.println("privateKey: " + privateKey);
            String originalMessage = "Hello, RSA!";
            String encryptedMessage = RsaUtil.encrypt(originalMessage, publicKey);
            String decryptedMessage = RsaUtil.decrypt(encryptedMessage, privateKey);
            System.out.println("Encrypted: " + encryptedMessage);
            System.out.println("Decrypted: " + decryptedMessage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
