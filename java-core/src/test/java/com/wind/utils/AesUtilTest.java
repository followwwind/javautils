package com.wind.utils;

import com.wind.utils.cipher.AesUtil;
import com.wind.utils.cipher.CipherUtil;
import org.junit.Test;

public class AesUtilTest {

    @Test
    public void testAes(){
        String str = "hello world";
        String key = "123";
        byte[] bytes = AesUtil.encrypt(str, key);
        String hexStr = CipherUtil.byte2Hex(bytes);
        System.out.println(hexStr);

        byte[] deByte = CipherUtil.hex2Byte(hexStr);
        String s = AesUtil.decrypt(deByte, key);
        System.out.println(s);
    }
}
