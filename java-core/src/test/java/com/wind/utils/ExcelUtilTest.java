package com.wind.utils;


import com.wind.utils.file.ExcelUtil;
import org.junit.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExcelUtilTest {


    @Test
    public void testWriteXls() throws IOException {
        String path = "d:/tmp/tmp.xls";
        List<List<String>> dataList = new ArrayList<>();
        dataList.add(Arrays.asList("id", "name", "age", "date"));
        dataList.add(Arrays.asList("1", "wind", "18", "2024-12-12"));
        ExcelUtil.write(path, dataList);
    }

    @Test
    public void testReadXls() throws IOException {
        String path = "d:/tmp/tmp.xls";
        List<List<String>> dataList = ExcelUtil.read(path);
        System.out.println(dataList);
    }

    @Test
    public void testWriteXlsx() throws IOException {
        String path = "d:/tmp/tmp.xlsx";
        List<List<String>> dataList = new ArrayList<>();
        dataList.add(Arrays.asList("id", "name", "age", "date"));
        dataList.add(Arrays.asList("1", "wind", "18", "2024-12-12"));
        ExcelUtil.write(path, dataList);
    }

    @Test
    public void testReadXlsx() throws IOException {
        String path = "d:/tmp/tmp.xlsx";
        List<List<String>> dataList = ExcelUtil.read(path);
        System.out.println(dataList);
    }
}
