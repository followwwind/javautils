package com.wind.utils;

import org.junit.Test;

public class RegexUtilTest {


    @Test
    public void testFloat(){
        System.out.println(RegexUtil.isFloat("1"));
        System.out.println(RegexUtil.isFloat("s123"));
        System.out.println(RegexUtil.isFloat("-0.1s"));
        System.out.println(RegexUtil.isFloat("1.1"));
        System.out.println(RegexUtil.isFloat("0.1"));
        System.out.println(RegexUtil.isFloat("-0.1"));
    }

    @Test
    public void testNumber(){
        System.out.println(RegexUtil.isNumber("1"));
        System.out.println(RegexUtil.isNumber("s123"));
        System.out.println(RegexUtil.isNumber("-0.1s"));
        System.out.println(RegexUtil.isNumber("1.1"));
        System.out.println(RegexUtil.isNumber("0.1"));
        System.out.println(RegexUtil.isNumber("-0.1"));
    }

    @Test
    public void testInteger(){
        System.out.println(RegexUtil.isInteger("1"));
        System.out.println(RegexUtil.isInteger("-1"));
        System.out.println(RegexUtil.isInteger("s123"));
        System.out.println(RegexUtil.isInteger("-0.1s"));
        System.out.println(RegexUtil.isInteger("1.1"));
        System.out.println(RegexUtil.isInteger("0.1"));
        System.out.println(RegexUtil.isInteger("-0.1"));
    }
}
