package com.wind.utils;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class DateUtilTest {

    @Test
    public void testLocalDateTime(){
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now);
        ZonedDateTime zoneId = now.atZone(ZoneId.of("Asia/Shanghai"));
        long second = zoneId.toEpochSecond();
        System.out.println(second);
        long milli = zoneId.toInstant().toEpochMilli();
        System.out.println(milli);
        System.out.println(new Date(milli));
    }
}
