package com.wind.utils;

import com.wind.utils.helper.EmailHelper;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class EmailHelperTest {


    @Test
    public void testHtml(){
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.host", "smtp.163.com");
        String sender = "*********@163.com";
        String receiver = "*********@qq.com";
        String title = "你好、Hello Mail";
        String content = "Hello World!!!";
        String username = "*********@163.com";
        String password = "*********";
        List<String> attachments = new ArrayList<>();
        attachments.add("src/main/resources/image/你好.txt");
        attachments.add("src/main/resources/image/code.png");
        EmailHelper email = new EmailHelper(username, password, props);
        email.sendHtml(sender, receiver, title, content, attachments);
    }

    @Test
    public void testQQ(){
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.qq.com");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465"); // 465或587，具体取决于邮箱服务器的要求
        props.put("mail.smtp.ssl.enable", "true"); // 使用SSL协议
        String sender = "*********@qq.com";
        String receiver = "*********@qq.com";
        String title = "你好、Hello Mail";
        String content = "Hello World!!!";
        String username = "*********@qq.com";
        String password = "*********";
        List<String> attachments = new ArrayList<>();
        attachments.add("src/main/resources/image/你好.txt");
        attachments.add("src/main/resources/image/code.png");
        EmailHelper email = new EmailHelper(username, password, props);
        email.sendHtml(sender, receiver, title, content, attachments);
    }
}
