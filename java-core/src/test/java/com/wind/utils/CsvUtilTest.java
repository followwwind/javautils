package com.wind.utils;

import com.wind.utils.file.CsvUtil;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvUtilTest {


    @Test
    public void testWrite() throws IOException {
        String csv = "d:/tmp/tmp.csv";
        List<List<String>> dataList = new ArrayList<>();
        dataList.add(Arrays.asList("id", "name", "age"));
        dataList.add(Arrays.asList("1", "wind", "18"));
        CsvUtil.write(csv, dataList);
    }

    @Test
    public void testRead() throws IOException {
        String csv = "d:/tmp/tmp.csv";
        List<List<String>> dataList = CsvUtil.read(csv);
        System.out.println(dataList);

        List<List<String>> dataList2 = CsvUtil.read(csv, true, GlobalConst.CHARSET.UTF8);
        System.out.println(dataList2);
    }

    @Test
    public void testCharset() throws IOException {
        System.out.println(GlobalConst.CHARSET.DEFAULT);
    }
}
