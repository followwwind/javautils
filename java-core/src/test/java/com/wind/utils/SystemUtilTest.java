package com.wind.utils;

import org.junit.Test;
import java.util.List;

public class SystemUtilTest {

    @Test
    public void testMemory(){
        List<Long> list = SystemUtil.getMemory();
        System.out.println(list);
        System.out.println(list.get(1) * 1.0/list.get(0));
    }

    @Test
    public void testDisk(){
        List<List<String>> list = SystemUtil.getDisk();
        for(List<String> s : list){
            System.out.println(s);
        }
    }

    @Test
    public void testCpu(){
        List<String> list = SystemUtil.getCpu();
        for(String s : list){
            System.out.println(s);
        }
    }

    @Test
    public void testSys(){
        SystemUtil.getMetric();
    }
}
