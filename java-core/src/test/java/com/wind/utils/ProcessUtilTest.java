package com.wind.utils;




import org.junit.Test;

import java.util.List;

public class ProcessUtilTest {

    @Test
    public void testExec(){
        String home = System.getenv("JAVA_HOME");
        System.out.println(home);
        List<List<String>> result = ProcessUtil.exec("java -version");
        System.out.println(result);

        List<List<String>> result2 = ProcessUtil.exec("python -V");
        System.out.println(result2);
    }
}
