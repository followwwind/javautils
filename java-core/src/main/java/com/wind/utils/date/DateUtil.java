package com.wind.utils.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *   日期工具类集合
 * </p>
 * @author wind
 * @date    2024-12-10 17:40
 * @version v1.0
 */
public class DateUtil {

    public final static String DEFAULT = "yyyy-MM-dd HH:mm:ss";
    public final static String MONTH = "yyyy-MM";
    public final static String DATE = "yyyy-MM-dd";
    public final static String DATE_HH = "yyyy-MM-dd HH";
    public final static String DATE_MM = "yyyy-MM-dd HH:mm";

    public final static String[] DEFAULT_FMT_ARR = new String[]{
            "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM-dd HH", "yyyy-MM-dd", "yyyy-MM", "yyyy",
            "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM/dd HH", "yyyy/MM/dd", "yyyy/MM"
    };


    /**
     * 格式化字符串日期，转换成Date
     * @param dateStr
     * @return
     */
    public static Date parse(String dateStr){
        for(String fmt : DEFAULT_FMT_ARR){
            Date date = parse(dateStr, fmt);
            if(date != null){
                return date;
            }
        }
        return null;
    }

    /**
     * 格式化字符串日期，转换成Date
     * @param date  字符串日期
     * @param pattern 默认 yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static Date parse(String date, String pattern){
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        Date d = null;
        try {
            d = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }

    /**
     * 日期按照指定格式转换成字符串
     * @param date  日期
     * @param pattern 默认 yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String format(Date date, String pattern){
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }


    /**
     * 返回当前时间day天之后（day>0）或day天之前（day<0）的时间
     * @param date 日期
     * @param day
     * @return
     */
    public static Date getDateD(Date date, int day){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime();
    }

    /**
     * 返回当前时间month个月之后（month>0）或month个月之前（month<0）的时间
     *
     * @param date
     * @param month
     * @return
     */
    public static Date getDateM(Date date, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        return calendar.getTime();
    }

    /**
     * 返回当前时间year年之后（year>0）或year年之前（year<0）的时间
     *
     * @param date
     * @param year
     * @return
     */
    public static Date getDateY(Date date, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, year);
        return calendar.getTime();
    }

    /**
     * 计算两个日期之间的天数差
     * @param start
     * @param end
     * @return
     * @throws ParseException
     */
    public static int getDays(Date start, Date end){
        return Integer.parseInt(String.valueOf((end.getTime() - start.getTime())/(1000*3600*24)));
    }

    /**
     * 获取两个日期
     * @param start
     * @param end
     * @return
     */
    public static List<Date> getDayList(Date start, Date end){
        List<Date> result = new ArrayList<>();
        Calendar tempStart = Calendar.getInstance();
        tempStart.setTime(start);
        tempStart.add(Calendar.DAY_OF_YEAR, 1);
        Calendar tempEnd = Calendar.getInstance();
        tempEnd.setTime(end);
        while (tempStart.before(tempEnd)) {
            result.add(tempStart.getTime());
            tempStart.add(Calendar.DAY_OF_YEAR, 1);
        }
        return result;
    }
}
