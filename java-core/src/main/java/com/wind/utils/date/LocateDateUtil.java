package com.wind.utils.date;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * <p>
 *    java.time库,LocalDateTime 不包含时区信息，因此在转换时必须明确指定时区。
 * </p>
 * @author wind
 * @date    2024/12/11 11:40
 * @version v1.0
 */
public class LocateDateUtil {


    /**
     * format
     * @param dateTime
     * @param fmt
     * @return
     */
    public static String format(LocalDateTime dateTime, String fmt){
        return dateTime.format(DateTimeFormatter.ofPattern(fmt));
    }

    /**
     * 解析日期字符串
     * @param dataTime
     * @param fmt
     * @return
     */
    public static LocalDateTime parse(String dataTime, String fmt){
        return LocalDateTime.parse(dataTime, DateTimeFormatter.ofPattern(fmt));
    }
}
