package com.wind.utils.media;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

/**
 * <p>
 *    图像处理工具类
 * </p>
 * @author wind
 * @date    2024-12-10 18:38
 * @version v1.0
 */
public class ImageUtil {
    /**
     * 读取图片，获取BufferedImage对象
     * @param fileName
     * @return
     */
    public static BufferedImage getImage(String fileName){
        File picture = new File(fileName);
        BufferedImage sourceImg = null;
        try {
            sourceImg = ImageIO.read(Files.newInputStream(picture.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sourceImg;
    }

    /**
     * 读取图片，获取ImageReader对象
     * @param fileName
     * @return
     */
    public static ImageReader getImageReader(String fileName){
        if(fileName != null){
            String suffix = "";
            for(String str : ImageIO.getReaderFormatNames()){
                if(fileName.lastIndexOf("." + str) > 0){
                    suffix = str;
                }
            }
            if(!"".equals(suffix)){
                try {
                    // 将FileInputStream 转换为ImageInputStream
                    ImageInputStream iis = ImageIO.createImageInputStream(Files.newInputStream(Paths.get(fileName)));
                    // 根据图片类型获取该种类型的ImageReader
                    Iterator<ImageReader> readers = ImageIO.getImageReadersByFormatName(suffix);
                    ImageReader reader = readers.next();
                    reader.setInput(iis, true);
                    return reader;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 图片截取 Graphic通过画布绘画
     * @param source 源图片
     * @param dest 目标
     * @param rect 待截取区域的坐标位置
     */
    public static void cutImageByGraphic(String source, String dest, Rectangle rect) throws Exception {
        // 用ImageIO读取字节流
        BufferedImage bufferedImage = getImage(source);
        int width = rect.width;
        int height = rect.height;
        int x = rect.x;
        int y = rect.y;
        BufferedImage destImage = new BufferedImage(width, height, Transparency.TRANSLUCENT);
        Graphics g = destImage.getGraphics();
        g.drawImage(bufferedImage, 0, 0, width, height, x, y, x + width, height + y, null);
        ImageIO.write(destImage, "jpg", new File(dest));
    }

    /**
     * 图片截取 ImageReader截取 速度比Graphic通过画布绘画快很多
     * @param source 源图片
     * @param rect 待截取区域的坐标位置
     * @return
     */
    public static void cutImage(String source, String dest, Rectangle rect) {
        BufferedImage bi = null;
        try {
            ImageReader reader = getImageReader(source);
            ImageReadParam param = reader.getDefaultReadParam();
            param.setSourceRegion(rect);
            bi = reader.read(0, param);
            ImageIO.write(bi, "jpg", new File(dest));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 图片压缩
     * @param source
     * @param dest
     * @param width
     * @param height
     * @throws Exception
     */
    public static void compress(String source, String dest, int width, int height) throws Exception {
        File input = new File(source);
        BufferedImage image = ImageIO.read(input);
        Image scaledImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage outputImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = outputImage.createGraphics();
        g2d.drawImage(scaledImage, 0, 0, null);
        g2d.dispose();
        ImageIO.write(outputImage, "jpg", new File(dest));
    }
}

