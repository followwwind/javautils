package com.wind.utils.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *    HttpUrlUtil工具类
 * </p>
 * @author wind
 * @date    2024-12-10 17:55
 * @version v1.0
 */
public class HttpUrlUtil {

    public static final String REQUEST_POST = "POST";
    public static final String REQUEST_GET = "GET";

    public static final String CONTENT_TYPE = "Content-Type";

    public static final String CONTENT_JSON = "application/json";

    private HttpUrlUtil(){
        
    }

    /**
     * 获取json header
     * @return
     */
    public static Map<String, String> getJsonHeader(){
        Map<String, String> map = new HashMap<>();
        map.put(CONTENT_TYPE, CONTENT_JSON);
        return map;
    }

    /**
     * 发送GET请求并返回响应内容
     * @param urlStr 请求的URL地址
     * @return 响应的内容
     * @throws IOException 如果发生I/O错误
     */
    public static String get(String urlStr, Map<String, String> headers) throws Exception {
        return request(urlStr, REQUEST_GET, headers, null);
    }

    /**
     * 发送POST请求并返回响应内容
     *
     * @param urlStr 请求的URL地址
     * @param headers 请求的URL地址
     * @param data 请求参数
     * @return 响应的内容
     * @throws IOException 如果发生I/O错误
     */
    public static String post(String urlStr, Map<String, String> headers, String data) throws Exception {
        return request(urlStr, REQUEST_POST, headers, data);
    }

    /**
     * 请求
     * @param urlStr
     * @param method
     * @param headers
     * @param data
     * @return
     * @throws Exception
     */
    public static String request(String urlStr, String method, Map<String, String> headers, String data) throws Exception{
        try {
            URL url = new URL(urlStr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(method);

            if(headers != null){
                headers.forEach(connection::setRequestProperty);
            }

            if(REQUEST_POST.equals(method) && data != null){
                connection.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(data);
                wr.flush();
                wr.close();
            }
            int responseCode = connection.getResponseCode();
            // success
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), StandardCharsets.UTF_8));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                return response.toString();
            }
            throw new RuntimeException("request exception, response code:" + responseCode);
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }
}
