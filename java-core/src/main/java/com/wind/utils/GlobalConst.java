package com.wind.utils;

import java.io.File;
import java.nio.charset.Charset;

/**
 * <p>
 *   全局常量
 * </p>
 * @author wind
 * @date    2024/12/11 16:18
 * @version v1.0
 */
public class GlobalConst {

    private GlobalConst(){
        
    }


    /**
     * 字符
     */
    public static final class CHARSET {
        public static final String UTF8 = "UTF-8";
        public static final String GBK = "GBK";

        public static final String DEFAULT = Charset.defaultCharset().name();
    }

    /**
     * 常用字符串常量
     */
    public static final class STR {

        private STR(){

        }

        public static final String POINT_STR = ".";

        public static final String BLANK_STR = "";

        public static final String SPACE_STR = " ";

        public static final String SYS_SEPARATOR = File.separator;

        public static final String FILE_SEPARATOR = "/";

        public static final String BRACKET_LEFT = "[";

        public static final String BRACKET_RIGHT = "]";
    }
}
