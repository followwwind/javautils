package com.wind.utils;

import java.util.Collection;

/**
 * <p>
 *     参数工具类
 * </p>
 * @author wind
 * @date    2024-12-11 9:20
 * @version v1.0
 */
public class ParamUtil {

    private ParamUtil(){

    }

    /**
     * 不能为null
     * @param t
     * @param msg
     * @return
     * @param <T>
     */
    public static <T> T notNull(T t, String msg){
        if(t == null){
            throw new IllegalArgumentException(msg);
        }
        return t;
    }

    /**
     * 不能为空，对象不能为null，字符串不能为null，也不能为空字符串
     * 集合不能为null，也不能为空集合
     * @param t
     * @param msg
     * @return
     * @param <T>
     */
    public static <T> T notEmpty(T t, String msg){
        if(t == null){
            throw new IllegalArgumentException(msg);
        }
        if(t instanceof String){
            String s = (String) t;
            isThrow("".equals(s.trim()), msg);
        }
        if(t instanceof Collection){
            Collection<?> s = (Collection<?>) t;
            isThrow(s.isEmpty(), msg);
        }
        return t;
    }

    /**
     * 若flag等于true，则抛出参数异常
     * @param flag
     * @param msg
     */
    public static void isThrow(boolean flag, String msg){
        if(flag){
            throw new IllegalArgumentException(msg);
        }
    }
}
