package com.wind.utils;

import java.util.regex.Pattern;

/**
 * 正则表达式工具类
 * @author wind
 */
public class RegexUtil {

    private static final String EMAIL = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

    /**中文正则表达式验证*/
    private static final String CHINESE = "^[\u4e00-\u9fa5]*$";

    /** 任意整数（包括0） */
    private static final String INTEGER = "-?\\d+";

    /** 任意浮点数（包括0） */
    private static final String FLOAT = "-?\\d+(\\.\\d+)";

    /**
     * 任意数字（包括0，整数，浮点数）
     */
    private static final String NUMBER = "-?\\d+(\\.\\d+)?";

    /**
     * 校验邮箱
     * @param email 邮箱
     * @return
     */
    public static boolean isEmail(String email){
        boolean flag = false;
        if(email != null){
            flag = email.matches(EMAIL);
        }

        return flag;
    }


    /**
     * 校验中文
     * @param chinese
     * @return
     */
    public static boolean isChinese(String chinese){
        boolean flag = false;
        if(chinese != null){
            flag = chinese.matches(CHINESE);
        }
        return flag;
    }

    /**
     * 任意整数（包括0）
     * @param value
     * @return
     */
    public static boolean isInteger(String value) {
        return match(value, INTEGER);
    }

    /**
     * 判断是否为数字
     * @param value
     * @return
     */
    public static boolean isNumber(String value){
        Pattern pattern = Pattern.compile(NUMBER);
        return pattern.matcher(value).matches();
    }

    /**
     * 任意浮点数（包括0）
     * @param value
     * @return
     */
    public static boolean isFloat(String value) {
        return match(value, FLOAT);
    }

    /**
     * 正则表达式匹配
     * @param value
     * @param regex
     * @return
     */
    public static boolean match(String value, String regex){
        Pattern pattern = Pattern.compile(regex);
        return pattern.matcher(value).matches();
    }
}
