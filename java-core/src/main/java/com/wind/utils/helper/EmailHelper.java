package com.wind.utils.helper;

import java.io.UnsupportedEncodingException;
import java.util.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;



/**
 * 邮件发送工具
 * 1．SMTP（递送邮件机制）
 * 简单邮件传输协议
 * SMTP服务器将邮件转发到接收者的SMTP服务器，直至最后被接收者通过POP或者IMAP协议获取。
 * 2．POP（获取邮件机制）
 * 邮局协议，目前为第3个版本POP3
 * 3．IMAP（多目录共享）
 * 接收信息的高级协议，目前版本为第4版IMAP4
 * 接收新信息，将这些信息递送给用户，维护每个用户的多个目录。
 * 4．MIME
 * 邮件扩展内容格式：信息格式、附件格式等等
 * 5．NNTP
 * 第三方新闻组协议
 * @author wind
 *
 */
public class EmailHelper {

	/**
	 * 账号
	 */
	private final String username;

	/**
	 * 密码
	 */
	private final String password;

	/**
	 * 配置
	 */
	private final Properties props;


	public EmailHelper(String username, String password, Properties props) {
		this.username = username;
		this.password = password;
		this.props = props;
	}

	/**
	 * 鉴权
	 * @return
	 */
	public Authenticator auth(){
		return new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		};
	}

	/**
	 * 获取session
	 * @return
	 */
	public Session getSession(){
		return Session.getInstance(props, auth());
	}

	/**
	 * 发送邮件
	 * @param sender
	 * @param receiver
	 * @param title
	 * @param content
	 * @param attaches
	 */
	public void sendHtml(String sender, String receiver, String title, String content, List<String> attaches) {
		// 根据属性新建一个邮件会话
		Session session = getSession();
		// 由邮件会话新建一个消息对象
		MimeMessage message = new MimeMessage(session);
		try {
			// 设置发件人的地址
			message.setFrom(new InternetAddress(sender));
			// 设置收件人,并设置其接收类型为TO
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiver));
			// 设置标题
			message.setSubject(title);
			// 设置信件内容 发送 纯文本 邮件 todo
			// message.setText(mailContent);
			// 发送HTML邮件，内容样式比较丰富
			//message.setContent(mailContent, "text/html;charset=utf-8");
			// 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
            Multipart multipart = new MimeMultipart("mixed");
            // 设置邮件的文本内容
            BodyPart contentPart = new MimeBodyPart();
            //contentPart.setText(content);
            contentPart.setContent(content,"text/html;charset=utf-8");
            multipart.addBodyPart(contentPart);
            for(String attach : attaches){
				DataSource dataSource = new FileDataSource(attach);
            	// 添加附件
                BodyPart messageAttaches = new MimeBodyPart();
                // 添加附件的内容
				messageAttaches.setDataHandler(new DataHandler(dataSource));
                // 添加附件的标题
				messageAttaches.setFileName(MimeUtility.encodeText(dataSource.getName()));
                multipart.addBodyPart(messageAttaches);
            }
            // 将multipart对象放到message中
            message.setContent(multipart);
			// 设置发信时间
			message.setSentDate(new Date());
			// 存储邮件信息
			message.saveChanges();
			// 发送邮件
			Transport.send(message);
		} catch (UnsupportedEncodingException | MessagingException e) {
			e.printStackTrace();
		}
	}
}
